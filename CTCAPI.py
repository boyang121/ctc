import requests, json

s = requests.Session() 
# all cookies received will be stored in the session object
ESN = '5474001003'
deviceID = ''

paramWriteURL = ''
idFromESNURL = 'https://connect.calamp.com/connect/services/devices/esn/%s?status=Enabled&calamp-services-app=d8b98aad-cc6e-4899-a86d-e2848eaf03b4&Cookie=UkRVYkxoM2xIRmlVUEhxQ2lZejdMdz09OnFJQmI0ZzlIMEVDQWdDbUNGOE52eWc9PQ'%ESN
authURL = 'https://connect.calamp.com/connect/services/login?useAuthToken=true&username=validation2&password=CalAmp!2'



print(s.post(authURL)) #should return back reponse 200. From this instance the cookie will expire after 30 minutes, but if program is re-run, another cookie is generated since the post executes again

### get the device ID. Temporary messy solution will fix later
response = s.get(idFromESNURL)
jsonData = json.loads(response.text)
temp = jsonData['response']
deviceID = (((temp['results'])[0])['device'])['id']
print(deviceID)

# write param 2560 index 0 to be value of 17
paramWriteURL = 'https://connect.calamp.com/connect/services/devicecommands/device/449734/parameterconfig/2560/0/0x00000011?v=2.0'
print(s.post(paramWriteURL)) ## this in theory should return back 200 for successful write, but for some reason is returning back 401 which is unauthorized, even though the cookie is passed along with.
#if cookie wasn't passed in or there was really an authorization failure, then the GET for the device ID should have failed as well, which leads me to suspect CTC side issue.




